﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GeckoFxDemoApplication
{
    public partial class FormInitXulPath : Form
    {
        public FormInitXulPath()
        {
            InitializeComponent();
        }

        private void buttonChooseFolder_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialogXul.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBoxXulFolder.Text = folderBrowserDialogXul.SelectedPath;
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (!System.IO.Directory.Exists(textBoxXulFolder.Text))
            {
                this.DialogResult = System.Windows.Forms.DialogResult.None;
                MessageBox.Show("Folder path not exists!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
    }
}
