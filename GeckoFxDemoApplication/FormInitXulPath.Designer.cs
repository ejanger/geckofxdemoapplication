﻿namespace GeckoFxDemoApplication
{
    partial class FormInitXulPath
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.folderBrowserDialogXul = new System.Windows.Forms.FolderBrowserDialog();
            this.textBoxXulFolder = new System.Windows.Forms.TextBox();
            this.buttonChooseFolder = new System.Windows.Forms.Button();
            this.buttonOk = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // folderBrowserDialogXul
            // 
            this.folderBrowserDialogXul.Description = "Choose xul.dll folder";
            this.folderBrowserDialogXul.RootFolder = System.Environment.SpecialFolder.MyComputer;
            this.folderBrowserDialogXul.ShowNewFolderButton = false;
            // 
            // textBoxXulFolder
            // 
            this.textBoxXulFolder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxXulFolder.Location = new System.Drawing.Point(0, 0);
            this.textBoxXulFolder.Name = "textBoxXulFolder";
            this.textBoxXulFolder.Size = new System.Drawing.Size(753, 20);
            this.textBoxXulFolder.TabIndex = 1;
            // 
            // buttonChooseFolder
            // 
            this.buttonChooseFolder.AutoSize = true;
            this.buttonChooseFolder.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonChooseFolder.Location = new System.Drawing.Point(0, 26);
            this.buttonChooseFolder.Name = "buttonChooseFolder";
            this.buttonChooseFolder.Size = new System.Drawing.Size(753, 23);
            this.buttonChooseFolder.TabIndex = 2;
            this.buttonChooseFolder.Text = "Choose XUL.dll folder...";
            this.buttonChooseFolder.UseVisualStyleBackColor = true;
            this.buttonChooseFolder.Click += new System.EventHandler(this.buttonChooseFolder_Click);
            // 
            // buttonOk
            // 
            this.buttonOk.AutoSize = true;
            this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOk.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonOk.Location = new System.Drawing.Point(0, 49);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(753, 23);
            this.buttonOk.TabIndex = 3;
            this.buttonOk.Text = "OK";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // FormInitXulPath
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(753, 72);
            this.Controls.Add(this.textBoxXulFolder);
            this.Controls.Add(this.buttonChooseFolder);
            this.Controls.Add(this.buttonOk);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FormInitXulPath";
            this.Text = "FormInitXulPath";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialogXul;
        private System.Windows.Forms.Button buttonChooseFolder;
        private System.Windows.Forms.Button buttonOk;
        internal System.Windows.Forms.TextBox textBoxXulFolder;
    }
}