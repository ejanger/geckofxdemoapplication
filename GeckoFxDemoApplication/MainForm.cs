﻿using Gecko;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GeckoFxDemoApplication
{
    public partial class MainForm : Form
    {
        private GeckoWebBrowser WebBrowser;
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            FormInitXulPath form = new FormInitXulPath();
            if (form.ShowDialog(this) != System.Windows.Forms.DialogResult.OK)
            {
                MessageBox.Show("Error choose folder xul.dll! Exit...", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
                return;
            }
            GeckoFxInit(form.textBoxXulFolder.Text);
        }

        private void GeckoFxInit(string xulrunnerDirPath)
        {
            if (!xulrunnerDirPath.Last().Equals('\\'))
            {
                xulrunnerDirPath += '\\';
            }
            Xpcom.Initialize(xulrunnerDirPath);
            this.WebBrowser = new GeckoWebBrowser();
            this.WebBrowser.Dock = DockStyle.Fill;
            this.WebBrowser.DocumentCompleted += WebBrowser_DocumentCompleted;
            splitContainer1.Panel1.Controls.Add(this.WebBrowser);
        }

        private void WebBrowser_DocumentCompleted(object sender, Gecko.Events.GeckoDocumentCompletedEventArgs e)
        {
            toolStripStatusLabel1.Text = "Document load end";

            GeckoElement newElement = WebBrowser.DomDocument.CreateElement(WebBrowser.DomDocument.DocumentElement.NamespaceURI, "rect");
            newElement.SetAttribute("x", "100");
            newElement.SetAttribute("y", "100");
            newElement.SetAttribute("height", "130");
            newElement.SetAttribute("width", "300");
            newElement.SetAttribute("style", @"fill:rgb(255,0,255);stroke-width:3;stroke:rgb(0,0,0)");
            WebBrowser.DomDocument.DocumentElement.AppendChild(newElement);

            newElement = WebBrowser.DomDocument.CreateElement(WebBrowser.DomDocument.DocumentElement.NamespaceURI, "rect");
            newElement.SetAttribute("x", "100");
            newElement.SetAttribute("y", "300");
            newElement.SetAttribute("height", "130");
            newElement.SetAttribute("width", "300");
            newElement.SetAttribute("style", @"fill:rgb(255,0,255);stroke-width:3;stroke:rgb(0,0,0)");
            WebBrowser.DomDocument.DocumentElement.AppendChild(newElement);
        }

        private void buttonLoadSvg_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                toolStripStatusLabel1.Text = "Start open file " + openFileDialog1.FileName;
                this.WebBrowser.Navigate(openFileDialog1.FileName);
            }
        }
    }
}
