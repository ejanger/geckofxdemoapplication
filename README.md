# README #

Простое WinForms приложение, использующее бибилотеку GeckoFx.

Демонстрирует [этот баг](https://bitbucket.org/geckofx/geckofx-33.0/issue/45/createelement-with-namespaceuri-error)

В решении добавлены ссылки на проекты Geckofx-Core и Geckofx-Winforms. Geckofx-Core был изменён согласно описанию в баге.